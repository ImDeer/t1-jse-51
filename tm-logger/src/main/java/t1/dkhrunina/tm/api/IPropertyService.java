package t1.dkhrunina.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getActiveMQHost();

    @NotNull
    String getActiveMQPort();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getMongoClientHost();

    @NotNull
    String getMongoClientPort();

}