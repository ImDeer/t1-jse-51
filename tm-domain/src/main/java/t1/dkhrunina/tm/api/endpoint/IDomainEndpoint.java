package t1.dkhrunina.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.data.*;
import t1.dkhrunina.tm.dto.response.data.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataLoadBase64Response loadBase64Data(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBase64Request request
    );

    @NotNull
    @WebMethod
    DataLoadBinaryResponse loadBinaryData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBinaryRequest request
    );

    @NotNull
    @WebMethod
    DataLoadJsonFasterXmlResponse loadJsonFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataLoadJsonJaxBResponse loadJsonJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataLoadXmlFasterXmlResponse loadXmlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataLoadXmlJaxBResponse loadXmlJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataLoadYamlFasterXmlResponse loadYamlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadYamlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataSaveBase64Response saveBase64Data(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBase64Request request
    );

    @NotNull
    @WebMethod
    DataSaveBinaryResponse saveBinaryData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBinaryRequest request
    );

    @NotNull
    @WebMethod
    DataSaveJsonFasterXmlResponse saveJsonFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataSaveJsonJaxBResponse saveJsonJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataSaveXmlFasterXmlResponse saveXmlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataSaveXmlJaxBResponse saveXmlJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataSaveYamlFasterXmlResponse saveYamlFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveYamlFasterXmlRequest request
    );

}