package t1.dkhrunina.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.comparator.CreatedComparator;
import t1.dkhrunina.tm.comparator.NameComparator;
import t1.dkhrunina.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<?> comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.getDisplayName().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public static String[] toValues() {
        @NotNull final String[] values = new String[Sort.values().length];
        int index = 0;
        for (@NotNull final Sort sort : Sort.values()) {
            values[index] = sort.getDisplayName();
            index++;
        }
        return values;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }
}