package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIdResponse extends AbstractResultResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectChangeStatusByIdResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    public ProjectChangeStatusByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}