package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class TaskFindOneByIdResponse extends AbstractResultResponse {

    @Nullable
    private TaskDTO task;

    public TaskFindOneByIdResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

    public TaskFindOneByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}