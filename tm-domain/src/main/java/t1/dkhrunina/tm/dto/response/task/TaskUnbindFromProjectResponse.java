package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class TaskUnbindFromProjectResponse extends AbstractResultResponse {

    @NotNull
    private TaskDTO task;

    public TaskUnbindFromProjectResponse(@NotNull final TaskDTO task) {
        this.task = task;
    }

    public TaskUnbindFromProjectResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}