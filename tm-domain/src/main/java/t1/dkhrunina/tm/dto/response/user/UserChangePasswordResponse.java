package t1.dkhrunina.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractResultResponse {

    @Nullable
    private UserDTO user;

    public UserChangePasswordResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public UserChangePasswordResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}