package t1.dkhrunina.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserRegisterResponse extends AbstractResultResponse {

    @Nullable
    private UserDTO user;

    public UserRegisterResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public UserRegisterResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}