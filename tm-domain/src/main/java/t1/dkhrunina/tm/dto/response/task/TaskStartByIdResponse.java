package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class TaskStartByIdResponse extends AbstractResultResponse {

    @NotNull
    private TaskDTO task;

    public TaskStartByIdResponse(@NotNull final TaskDTO task) {
        this.task = task;
    }

    public TaskStartByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}