package t1.dkhrunina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.repository.model.IProjectRepository;
import t1.dkhrunina.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project>
        implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<Project> getEntityClass() {
        return Project.class;
    }

}