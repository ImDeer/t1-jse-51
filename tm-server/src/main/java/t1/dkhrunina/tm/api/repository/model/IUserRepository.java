package t1.dkhrunina.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

}