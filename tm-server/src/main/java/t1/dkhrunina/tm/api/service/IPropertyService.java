package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getActiveMQHost();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getDBCacheRegionFactory();

    @NotNull
    String getDBConfigFilePath();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getDBHbm2DdlAuto();

    @NotNull
    String getDBLazyLoadNoTransEnabled();

    @NotNull
    Boolean getDBLoggingEnabled();

    @NotNull
    String getDBSecondLevelCacheEnabled();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBRegionPrefix();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBUseMinPuts();

    @NotNull
    String getDBUseQueryCache();

    @NotNull
    String getDBUser();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}