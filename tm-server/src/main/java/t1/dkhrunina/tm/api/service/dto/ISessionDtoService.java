package t1.dkhrunina.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {

    SessionDTO remove(@Nullable SessionDTO session);

    List<SessionDTO> findAll();

}