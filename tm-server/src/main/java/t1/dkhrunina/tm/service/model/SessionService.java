package t1.dkhrunina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.model.ISessionRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.model.ISessionService;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @Nullable final List<Session> sessions;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            sessions = repository.findAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public Session remove(@Nullable final Session session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

}