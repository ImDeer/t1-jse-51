package t1.dkhrunina.tm.repository.model;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.model.IProjectRepository;
import t1.dkhrunina.tm.api.repository.model.ITaskRepository;
import t1.dkhrunina.tm.api.repository.model.IUserRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepositoryTest extends AbstractSchemeTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User testUser1;

    @NotNull
    private User testUser2;

    @NotNull
    private Project project1;

    @NotNull
    private Project project2;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        taskRepository = new TaskRepository(entityManager);
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        testUser1 = new User();
        testUser2 = new User();
        userRepository.add(testUser1);
        userRepository.add(testUser2);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        project1 = new Project();
        project2 = new Project();
        projectRepository.add(project1);
        projectRepository.add(project2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task " + i);
            task.setDescription("descr " + i);
            if (i < 5) {
                task.setUser(testUser1);
                task.setProject(project1);
            } else {
                task.setUser(testUser2);
                task.setProject(project2);
            }
            taskList.add(task);
            taskRepository.add(task);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String taskName = "name";
        @NotNull final String taskDescription = "descr";
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUser(testUser1);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task createdTask = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(testUser1, createdTask.getUser());
        Assert.assertEquals(taskName, createdTask.getName());
        Assert.assertEquals(taskDescription, createdTask.getDescription());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = taskRepository.getSize() + 2;
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String firstTaskName = "task name 1";
        @NotNull final String firstTaskDescription = "task descr 1";
        @NotNull final Task firstTask = new Task();
        firstTask.setName(firstTaskName);
        firstTask.setDescription(firstTaskDescription);
        tasks.add(firstTask);
        @NotNull final String secondTaskName = "task name 2";
        @NotNull final String secondTaskDescription = "task descr 2";
        @NotNull final Task secondTask = new Task();
        secondTask.setName(secondTaskName);
        secondTask.setDescription(secondTaskDescription);
        tasks.add(secondTask);
        @NotNull final Collection<Task> addedTasks = taskRepository.add(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear(testUser1.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(testUser1.getId()));
    }

    @Test
    public void testFindAll() {
        @Nullable final List<Task> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull List<Task> taskListForProject = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .filter(m -> project1.equals(m.getProject()))
                .collect(Collectors.toList());
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(testUser1.getId(), project1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForProject, tasks);
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = taskRepository.findAll().size();
        @Nullable List<Task> tasks = taskRepository.findAll(comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(allProjectsSize, tasks.size());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable final List<Task> tasks = taskRepository.findAll(testUser1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .filter(m -> project1.equals(m.getProject()))
                .collect(Collectors.toList());
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<Task> tasks = taskRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    public void testFindOneById() {
        @Nullable Task task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final Task foundTask = taskRepository.findOneById("meow");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Task task;
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 0; i < taskListForUser.size(); i++) {
            task = taskListForUser.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(testUser1.getId(), taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        @Nullable final Task foundTask = taskRepository.findOneById(testUser1.getId(), "meow");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(testUser1.getId(), null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 1; i <= taskList.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByIndexNull() {
        @Nullable final Task task = taskRepository.findOneByIndex(null);
        Assert.assertNull(task);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= taskListForUser.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(testUser1.getId(), i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        @Nullable final Task task = taskRepository.findOneByIndex(testUser1.getId(), null);
        Assert.assertNull(task);
    }

    @Test
    public void testGetSize() {
        int actualSize = taskRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .count();
        int actualSize = taskRepository.getSize(testUser1.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final Task task = taskList.get(0);
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable final Task deletedTask = taskRepository.remove(task);
        Assert.assertNotNull(deletedTask);
        @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(taskId);
        Assert.assertNull(deletedTaskInRepository);
    }

    @Test
    public void testRemoveNull() {
        @Nullable final Task task = taskRepository.remove(null);
        Assert.assertNull(task);
    }

}