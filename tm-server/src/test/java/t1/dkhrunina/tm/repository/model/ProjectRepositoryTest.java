package t1.dkhrunina.tm.repository.model;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.repository.model.IProjectRepository;
import t1.dkhrunina.tm.api.repository.model.IUserRepository;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private static EntityManager entityManager;

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User testUser1;

    @NotNull
    private User testUser2;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        projectRepository = new ProjectRepository(entityManager);
        testUser1 = new User();
        testUser2 = new User();
        userRepository.add(testUser1);
        userRepository.add(testUser2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project  " + i);
            project.setDescription("descr  " + i);
            if (i < 5) project.setUser(testUser1);
            else project.setUser(testUser2);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String projectName = "name";
        @NotNull final String projectDescription = "descr";
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUser(testUser1);
        @Nullable final Project createdProject = projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(testUser1, createdProject.getUser());
        Assert.assertEquals(projectName, createdProject.getName());
        Assert.assertEquals(projectDescription, createdProject.getDescription());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = projectRepository.getSize() + 2;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String firstProjectName = "project name 1";
        @NotNull final String firstProjectDescription = "project descr 1";
        @NotNull final Project firstProject = new Project();
        firstProject.setName(firstProjectName);
        firstProject.setDescription(firstProjectDescription);
        firstProject.setUser(testUser1);
        projects.add(firstProject);
        @NotNull final String secondProjectName = "project name 2";
        @NotNull final String secondProjectDescription = "project descr 2";
        @NotNull final Project secondProject = new Project();
        secondProject.setName(secondProjectName);
        secondProject.setDescription(secondProjectDescription);
        secondProject.setUser(testUser1);
        projects.add(secondProject);
        @NotNull final Collection<Project> addedProjects = projectRepository.add(projects);
        Assert.assertTrue(addedProjects.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        projectRepository.clear(testUser1.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(testUser1.getId()));
    }

    @Test
    public void testFindAll() {
        int allProjectsSize = projectRepository.getSize();
        @Nullable final List<Project> projects = projectRepository.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectsSize, projects.size());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = projectRepository.findAll().size();
        @NotNull List<Project> projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable final List<Project> projects = projectRepository.findAll(testUser1.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    public void testFindAllWithComparatorFor() {
        @NotNull final List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<Project> projects = projectRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(testUser1.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    public void testFindOneById() {
        @Nullable Project project;
        for (int i = 1; i <= projectList.size(); i++) {
            project = projectList.get(i - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final Project foundProject = projectRepository.findOneById("meow");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectListForUser) {
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(testUser1.getId(), projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        @Nullable final Project foundProject = projectRepository.findOneById(testUser1.getId(), "meow");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(testUser1.getId(), null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 1; i <= projectList.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByIndexNull() {
        @Nullable final Project project = projectRepository.findOneByIndex(null);
        Assert.assertNull(project);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= projectListForUser.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(testUser1.getId(), i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        @Nullable final Project project = projectRepository.findOneByIndex(testUser1.getId(), null);
        Assert.assertNull(project);
    }

    @Test
    public void testGetSize() {
        int actualSize = projectRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .count();
        int actualSize = projectRepository.getSize(testUser1.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final Project project = projectList.get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final Project deletedProject = projectRepository.remove(project);
        Assert.assertNotNull(deletedProject);
        @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(projectId);
        Assert.assertNull(deletedProjectInRepository);
    }

    @Test
    public void testRemoveNull() {
        @Nullable final Project project = projectRepository.remove(null);
        Assert.assertNull(project);
    }

}