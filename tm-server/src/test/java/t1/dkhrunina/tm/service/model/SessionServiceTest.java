package t1.dkhrunina.tm.service.model;

import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.AbstractSchemeTest;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.api.service.model.IProjectTaskService;
import t1.dkhrunina.tm.api.service.model.ISessionService;
import t1.dkhrunina.tm.api.service.model.IUserService;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private User user;

    @NotNull
    private User admin;

    @NotNull
    private IUserService userService;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        sessionService = new SessionService(connectionService);
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final Session session1 = new Session();
        session1.setUser(user);
        session1.setRole(user.getRole());
        sessionService.add(session1);
        @NotNull final Session session2 = new Session();
        session2.setUser(admin);
        session2.setRole(admin.getRole());
        sessionService.add(session2);
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
    }

    @After
    public void afterTest() {
        sessionService.clear(user.getId());
        sessionService.clear(admin.getId());
        userService.remove(user);
        userService.remove(admin);
    }

    @Test
    public void testAddForUser() {
        int expectedNumberOfEntries = sessionService.getSize(user.getId()) + 1;
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setRole(user.getRole());
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testAddForUserNull() {
        int expectedNumberOfEntries = sessionService.getSize(user.getId());
        @Nullable final Session session = sessionService.add(null);
        Assert.assertNull(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize(user.getId()) > 0);
        sessionService.clear(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testExistById() {
        for (@NotNull final Session session : sessionList) {
            final boolean isExist = sessionService.existsById(session.getUser().getId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            final boolean isExist = sessionService.existsById(user.getId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUserFalse() {
        final boolean isExist = sessionService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable final List<Session> sessions = sessionService.findAll(user.getId());
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionsForTestUser, sessions);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Session foundSession;
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            foundSession = sessionService.findOneById(user.getId(), session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void testFindOneByIdForUserNull() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(user.getId(), null));
    }

    @Test
    public void testFindOneByIdForUserEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(user.getId(), ""));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionsForTestUser.size(); i++) {
            @Nullable final Session session = sessionService.findOneByIndex(user.getId(), i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void testFindOneByIndexIncorrectForUser() {
        int index = sessionService.getSize(user.getId()) + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), index));
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), -1));
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .count();
        int actualSize = sessionService.getSize(user.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.remove(session);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(session.getUser().getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void testRemoveEntityNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.remove(null));
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.removeById(user.getId(), sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(user.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void testRemoveByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(user.getId(), null));
    }

    @Test
    public void testRemoveByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(user.getId(), ""));
    }

    @Test
    public void testRemoveByIdNotFoundForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.removeById(user.getId(), "meow"));
    }

    @Test
    public void testRemoveByIndexForUser() {
        int index = (int) sessionList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .count();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionService.removeByIndex(user.getId(), index);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(user.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalidForUser() {
        int index = sessionList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), index));
    }

    @Test
    public void testRemoveByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveByIndexNegaiveForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), -1));
    }

}