package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Role;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    UserDTO register(@NotNull String login, @NotNull String password, @NotNull String email);

    void login(@Nullable String login, @Nullable String password);

    UserDTO check(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    UserDTO getUser();

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}