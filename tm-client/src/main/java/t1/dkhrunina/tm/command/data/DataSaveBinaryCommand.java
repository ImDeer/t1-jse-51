package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataSaveBinaryRequest;

public final class DataSaveBinaryCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-s-bin";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveBinaryData(new DataSaveBinaryRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}