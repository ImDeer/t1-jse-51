package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.endpoint.ITaskEndpoint;
import t1.dkhrunina.tm.command.AbstractCommand;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.enumerated.Status;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + Status.toName(task.getStatus()));
        System.out.println("Project id: " + (task.getProjectId() == null ? "not bound" : task.getProjectId()));
    }

    protected void renderTasks(@Nullable final List<TaskDTO> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}