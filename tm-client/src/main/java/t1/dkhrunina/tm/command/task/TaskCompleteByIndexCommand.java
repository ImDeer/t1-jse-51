package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskCompleteByIndexRequest;
import t1.dkhrunina.tm.dto.response.task.TaskCompleteByIndexResponse;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-complete-by-index";

    @NotNull
    private static final String DESCRIPTION = "Complete task by index.";

    @Override
    public void execute() {
        System.out.println("[Complete task by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken(), index);
        @NotNull final TaskCompleteByIndexResponse response = getTaskEndpoint().completeTaskByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}