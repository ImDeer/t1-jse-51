package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.user.UserLoginRequest;
import t1.dkhrunina.tm.dto.response.user.UserLoginResponse;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-login";

    @NotNull
    private static final String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[UserDTO login]");
        System.out.println("Enter login: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password: ");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse response = getAuthEndpoint().loginUser(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        @Nullable final String token = response.getToken();
        setToken(token);
        System.out.println(token);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}